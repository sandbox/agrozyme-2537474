<?php
namespace Drupal\embed_facebook_video\Media;

use Drupal\mixin\Filter;
use Drupal\mixin\SettingExport;

class Wrapper extends SettingExport {
  protected $name = '';
  protected $class = '';
  protected $description = '';
  protected $type = '';

  public function setClass($value) {
    $this->class = Filter::sanitize(Filter::TYPE_STRING, $value);
    return $this;
  }

  public function setDescription($value) {
    $this->description = Filter::sanitize(Filter::TYPE_STRING, $value);
    return $this;
  }

  public function setName($value) {
    $this->name = Filter::sanitize(Filter::TYPE_STRING, $value);
    return $this;
  }

  public function setType($value) {
    $this->type = Filter::sanitize(Filter::TYPE_STRING, $value);
    return $this;
  }

}
