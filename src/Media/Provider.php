<?php

namespace Drupal\embed_facebook_video\Media;

use Drupal\mixin\Filter;
use Drupal\mixin\SettingExport;

class Provider extends SettingExport {
  protected $title = '';
  protected $image;
  protected $hidden;
  protected $weight;

  function setHidden($value = null) {
    $this->hidden = Filter::sanitize(Filter::TYPE_BOOLEAN, $value, true);
    return $this;
  }

  function setImage($value = null) {
    $this->image = Filter::sanitize(Filter::TYPE_STRING, $value, true);
    return $this;
  }

  function setTitle($value = '') {
    $this->title = Filter::sanitize(Filter::TYPE_STRING, $value);
    return $this;
  }

  function setWeight($value) {
    $this->weight = Filter::sanitize(Filter::TYPE_INTEGER, $value, true);
    return $this;
  }

}
