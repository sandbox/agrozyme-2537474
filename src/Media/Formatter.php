<?php

namespace Drupal\embed_facebook_video\Media;

use Drupal\mixin\Classes\Parser;
use Drupal\mixin\SettingExport;

abstract class Formatter extends SettingExport {
  protected $label = '';
  protected $file_types = [];
  protected $default_settings = [];
  protected $mime_types = [];
  protected $view_callback;
  protected $settings_callback;

  static function hookView($file, $display, $langcode) {
    return static::create()->doView($file, $display, $langcode);
  }

  static function create(array $data = []) {
    foreach (['view', 'settings'] as $item) {
      $data[$item . '_callback'] = static::getCallabckName($item);
    }

    return parent::create($data);
  }

  protected function exportMapping() {
    $mapping = [
      'file_types' => 'file types',
      'default_settings' => 'default settings',
      'mime_types' => 'mime types',
      'view_callback' => 'view callback',
      'settings_callback' => 'settings callback',
    ];

    return $mapping;
  }

  static function getCallabckName($action) {
    $module = Parser::create(get_called_class())->getModule();
    $name = static::getFormatterName();
    return implode('_', [$module, 'file_formatter', $name, $action]);
  }

  static function getFormatterName() {
    return drupal_strtolower(Parser::create(get_called_class())->getName());
  }

  static function hookSettings($form, &$form_state, $settings) {
    return static::create()->doSettings($form, $form_state, $settings);
  }

  function doView($file, $display, $langcode) {
    return [];
  }

  function doSettings($form, &$form_state, $settings) {
    return [];
  }

}
