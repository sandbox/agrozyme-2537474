<?php

namespace Drupal\embed_facebook_video\Media\Hook;

use Drupal\embed_facebook_video\Media\Formatter;
use Drupal\mixin\Classes\Parser;
use Drupal\mixin\Traits\Hook;

abstract class Formatters {
  use Hook;

  /** @var Formatter[] $hooks */
  protected $hooks = [];

  static function hookInfo() {
    return static::create()->doInfo();
  }

  function doInfo() {
    $items = [];

    foreach ($this->hooks as $item) {
      $module = Parser::create(get_class($item))->getModule();
      $name = $item::getFormatterName();
      $items += $item->exportProperties($module . '_' . $name);
    }

    return $items;
  }

  protected static function getHookMapping() {
    return static::create()->mapping();
  }

  function mapping() {
    $items = [];

    foreach ($this->hooks as $item) {
      $class = get_class($item);
      $module = Parser::create($class)->getModule();
      $name = $item::getFormatterName();
      $items[$class] = [
        'hookView' => $module . '_file_formatter_' . $name . '_view',
        'hookSettings' => $module . '_file_formatter_' . $name . '_settings',
      ];
    }

    $class = get_called_class();
    $module = Parser::create($class)->getModule();
    $items[$class] = ['hookInfo' => $module . '_file_formatter_info'];
    return $items;
  }

}
