<?php
namespace Drupal\embed_facebook_video\Media\Hook;

use Drupal\embed_facebook_video\Media\Provider as MediaProvider;
use Drupal\mixin\Classes\Parser;
use Drupal\mixin\Traits\Hook;

abstract class Providers {
  use Hook;

  /** @var MediaProvider[] $hooks */
  protected $hooks = [];

  static function hookProviders() {
    return static::create()->doProviders();
  }

  function doProviders() {
    $items = [];

    foreach ($this->hooks as $class => $item) {
      $items += $item->exportProperties($class);
    }

    return $items;
  }

  protected static function getHookMapping() {
    $class = get_called_class();
    $module = Parser::create($class)->getModule();
    $items = [];
    $items[$class] = [
      'hookProviders' => $module . '_media_internet_providers'
    ];

    return $items;
  }

}
