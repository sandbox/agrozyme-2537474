<?php

namespace Drupal\embed_facebook_video\Media\Hook;

use Drupal\embed_facebook_video\Media\Wrapper as MediaWrapper;
use Drupal\mixin\Classes\Parser;
use Drupal\mixin\Traits\Hook;

abstract class Wrappers {
  use Hook;

  /** @var MediaWrapper[] $hooks */
  protected $hooks = [];

  static function hookWrappers() {
    return static::create()->doWrappers();
  }

  function doWrappers() {
    $items = [];

    foreach ($this->hooks as $class => $item) {
      $items += $item->exportProperties($class);
    }

    return $items;
  }

  protected static function getHookMapping() {
    $class = get_called_class();
    $module = Parser::create($class)->getModule();
    $items = [];
    $items[$class] = [
      'hookWrappers' => $module . '_media_internet_video_stream_wrappers'
    ];

    return $items;
  }

}
