<?php
namespace Drupal\embed_facebook_video;

use Drupal\mixin\Classes\Parser;
use Drupal\mixin\Traits\Hook;

class Hooks {
  use Hook;

  static function hookCtoolsPluginApi($module, $api) {
    if ($module == 'file_entity' && $api == 'file_default_displays') {
      return ['version' => 1];
    }
  }

  static function hookFileMimetypeMappingAlter(&$mapping) {
    $mapping['mimetypes'][] = 'video/facebook';
  }

  protected static function getHookMapping() {
    $class = get_called_class();
    $module = Parser::create($class)->getModule();
    $items = [
      $class => [
        'hookCtoolsPluginApi' => $module . '_ctools_plugin_api',
        'hookFileMimetypeMappingAlter' => $module . '_file_mimetype_mapping_alter',
      ]
    ];

    return $items;
  }

}
