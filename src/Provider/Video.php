<?php

namespace Drupal\embed_facebook_video\Provider;

use MediaInternetBaseHandler as Base;

class Video extends Base {
  function claim($embed_code) {
    if ($this->parse($embed_code)) {
      return true;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, true);
    return $file;
  }

  function parse($embed_code) {
    $patterns = [
      '@facebook\.com/.*\/videos\/[a-z][a-z]\..*/(\d+)/.*@i',
      '@facebook\.com/video\.php\?v=(\d+)@i',
      '@facebook\.com/.*\/videos\/(\d+)\/.*@i',
      '@facebook\.com/.*\/posts\/(\d+)\/.*@i',
    ];

    foreach ($patterns as $pattern) {
      preg_match($pattern, $embed_code, $matches);
      if (isset($matches[1])) {
        return file_stream_wrapper_uri_normalize('facebook://v/' . $matches[1]);
      }
    }
  }

}
