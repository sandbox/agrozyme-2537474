<?php
namespace Drupal\embed_facebook_video;

use Drupal\embed_facebook_video\Formatter\Video;
use Drupal\embed_facebook_video\Media\Hook\Formatters as Base;

class Formatters extends Base {

  function __construct() {
    $this->hooks[] = Video::create();
  }
}
