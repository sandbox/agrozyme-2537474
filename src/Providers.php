<?php
namespace Drupal\embed_facebook_video;

use Drupal\embed_facebook_video\Media\Hook\Providers as Base;
use Drupal\embed_facebook_video\Media\Provider;
use Drupal\mixin\Classes\Parser;

//use Drupal\embed_facebook_video\Provider\Video;

class Providers extends Base {
  function __construct() {
    $module = Parser::create(get_called_class())->getModule();
    $class = Parser::getDrupalClassName($module, 'Provider\\Video');
    $this->hooks[$class] = Provider::create()->setTitle(t('Facebook Video'));
  }

}
