<?php
namespace Drupal\embed_facebook_video\Wrapper;

use MediaReadOnlyStreamWrapper;

class Video extends MediaReadOnlyStreamWrapper {
  protected $baseUrl = 'http://facebook.com';

  public static function getMimeType($uri, $mapping = null) {
    return 'video/facebook';
  }

  public function interpolateUrl() {
    $items = $this->get_parameters();

    if ($items) {
      return $this->baseUrl . '/' . $items['v'];
    }
  }

  public function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-facebook-video/' . check_plain($parts['v']) . '.jpg';
    return $local_path;
  }

}
