<?php
namespace Drupal\embed_facebook_video\Formatter;

use Drupal\embed_facebook_video\Media\Formatter as Base;

class Video extends Base {
  function doSettings($form, &$form_state, $settings) {
    dpm($form);
    $items = [];

    $items['width'] = [
      '#title' => t('Width'),
      '#type' => 'textfield',
      '#default_value' => $settings['width'],
      //'#element_validate' => array('_youtube_validate_video_width_and_height'),
    ];
    $items['height'] = [
      '#title' => t('Height'),
      '#type' => 'textfield',
      '#default_value' => $settings['height'],
      //'#element_validate' => array('_youtube_validate_video_width_and_height'),
    ];

    return $items;
  }

  function doView($file, $display, $langcode) {
    return parent::doView($file, $display, $langcode);
  }

  protected static function setupMethodPattern() {
    return ['setupProperties'];
  }

  function setupProperties() {
    $this->label = t('Embed Facebok Video');
    $this->file_types = ['video'];
    $this->mime_types = ['video/facebook'];
  }

}
