<?php

namespace Drupal\embed_facebook_video;

use Drupal\embed_facebook_video\Media\Hook\Wrappers as Base;
use Drupal\embed_facebook_video\Media\Wrapper;
use Drupal\mixin\Classes\Parser;

class Wrappers extends Base {
  function __construct() {
    $module = Parser::create(get_called_class())->getModule();
    $class = Parser::getDrupalClassName($module, 'Wrapper\\Video');
    $this->hooks['embed_facebook_video'] = Wrapper::create()
      ->setName(t('Embed Facebook Video'))
      ->setDescription(t('Remote videos hosted on the Facebook video-sharing website.'))
      ->setClass($class)
      ->setType(STREAM_WRAPPERS_READ_VISIBLE);
  }

}
